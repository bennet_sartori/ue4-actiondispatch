#include "ActionDispatch.h"

#include "Dispatch.h"

#include "RPCChannel.h"

UWorld* UDispatch::GetWorldFromActor(AActor* Actor)
{
    return Actor->GetWorld();
}

void UDispatch::ExecuteAction(UWorld* World, ActionExecutionType Type, FGameActionContext ActionContext)
{
    AGameActionDispatcher* dispatcher = GetGameActionDispatcher(World);
    if (dispatcher)
    {
        dispatcher->ExecuteAction(Type, ActionContext);
    }
}

AGameActionDispatcher* UDispatch::GetGameActionDispatcher(UWorld* World)
{
    for (TActorIterator<AGameActionDispatcher> it(World); it; ++it)
    {
        return *it;
    }

    return nullptr;
}

void UDispatch::SpawnRPCChannels(APlayerController* Player, TArray<TSubclassOf<ARPCChannel> > ChannelClasses)
{
    for (UClass* channelClass : ChannelClasses)
    {
        ARPCChannel* channel = Player->GetWorld()->SpawnActor<ARPCChannel>(channelClass);
        channel->SetOwningPlayer(Player);
    }
}

AGameActionDispatcher* UDispatch::SpawnDispatcher(UWorld* World)
{
    return World->SpawnActor<AGameActionDispatcher>(AGameActionDispatcher::StaticClass());
}
