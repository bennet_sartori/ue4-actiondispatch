#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"

#include "GameActionDispatcher.h"
#include "GameAction.h"

#include "Dispatch.generated.h"

class ARPCChannel;

/**
 * Function library for easy use of the dispatcher system.
 * Includes functions to setup the system as well as to use it.
 * Usable from Blueprints!
 */
UCLASS(ClassGroup = GameActionDispatch)
class ACTIONDISPATCH_API UDispatch : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:
    /**
     * Get the world the actor belongs to.
     *
     * @return Returns the world of the actor
     */
    UFUNCTION(BlueprintPure, Category = "GameActionDispatch")
    static UWorld* GetWorldFromActor(AActor* Actor);

    /**
     * Executes a specific GameAction using the worlds dispatcher.
     * Note: Dispatcher must be setup before.
     *
     * @param World The world including the dispatcher
     * @param Type Whether the action shall be executed locally only, on the server only or on both
     * @param ActionContext Context of the action to execute.
     *
     * @see AGameActionDispatcher::ExecuteAction()
     */
    UFUNCTION(BlueprintCallable, Category = "GameActionDispatch")
    static void ExecuteAction(UWorld* World, ActionExecutionType Type, FGameActionContext ActionContext);

    /**
     * Retrieve the dispatcher of the given world
     *
     * @param World The world including the dispatcher
     * @return A reference to the dispatcher
     *
     * @see AGameActionDispatcher
     */
    UFUNCTION(BlueprintPure, Category = "GameActionDispatch")
    static AGameActionDispatcher* GetGameActionDispatcher(UWorld* World);

    /**
     * Spawns RPC channels of the specified classes for the given player.
     * Should be called after player setup for each player.
     *
     * @param Player The player that will be the owner of the channel
     * @param ChannelClasses An array of subclasses of ARPCChannel that will be spawned
     */
    UFUNCTION(BlueprintCallable, Category = "GameActionDispatch")
    static void SpawnRPCChannels(APlayerController* Player, TArray<TSubclassOf<ARPCChannel> > ChannelClasses);

    /**
    * Spawns the central dispatcher of the world.
    * Call at game start.
    *
    * @param World The world to spawn the dispatcher into
    */
    UFUNCTION(BlueprintCallable, Category = "GameActionDispatch")
    static AGameActionDispatcher* SpawnDispatcher(UWorld* World);
};
