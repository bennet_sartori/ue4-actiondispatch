#pragma once

#include "Object.h"

#include "GameAction.generated.h"

/**
 * Different execution possibilities.
 * For actions involving server communication a RPCChannel has to be setup to make the server part working.
 */
UENUM(ClassGroup = GameActionDispatch)
enum ActionExecutionType
{
    /**
     * Execute the action on the same machine where it was dispatched.
     */
    Local,
    /**
     * Execute the action only on the server. If dispatched from a client
     * then it is send with via RPC channel. If dispatched from Server then
     * is behaves exactly like actions with type Local.
     */
    Server,
    /**
     * Executes the action on the local machine and also on the server.
     * Has the same requirements as actions with type Server.
     */
    Net,
    /**
     * Executes the action on a specific client only.
     * The client must be specified with FGameAction::targetPlayer().
     */
    Client,
    /**
     * Executes the action on all clients.
     * Can only be executed on the server.
     * A client can be excluded using FGameAction::excludePlayer().
     */
    Multicast,

    None
};

/**
 * Base class for all actions that can be dispatched.
 * Users should create their own subclass.
 */
USTRUCT()
struct ACTIONDISPATCH_API FGameAction
{
    GENERATED_USTRUCT_BODY()

public:
    virtual FName GetActionName() const PURE_VIRTUAL(FGameAction::GetActionName(), return ""; );
};
