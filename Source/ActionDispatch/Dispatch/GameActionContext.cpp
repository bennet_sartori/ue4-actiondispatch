#include "ActionDispatch.h"

#include "GameActionContext.h"

FGameActionContext::FGameActionContext() 
    : Action(nullptr)
    , ExecutedByPlayerId(PLAYER_ID_UNSPECIFIED)
    , TargetPlayerId(PLAYER_ID_UNSPECIFIED)
    , ExcludePlayerId(PLAYER_ID_UNSPECIFIED)
{

}

FGameActionContext::FGameActionContext(const FGameAction& InAction)
    : Action(&InAction)
    , ExecutedByPlayerId(PLAYER_ID_UNSPECIFIED)
    , TargetPlayerId(PLAYER_ID_UNSPECIFIED)
    , ExcludePlayerId(PLAYER_ID_UNSPECIFIED)
{

}

FGameActionContext& FGameActionContext::TargetPlayer(int32 InPlayerTargetId)
{
    TargetPlayerId = InPlayerTargetId;
    return *this;
}

FGameActionContext& FGameActionContext::ExcludePlayer(int32 InExcludePlayerId)
{
    ExcludePlayerId = InExcludePlayerId;
    return *this;
}

FGameActionContext& FGameActionContext::ExecutedByPlayer(int32 InExecutedByPlayer)
{
    ExecutedByPlayerId = InExecutedByPlayer;
    return *this;
}
