#pragma once

#include "GameAction.h"

#include "GameActionContext.generated.h"

/**
 * Context wrapper for a dispatched game action.
 * Is used by all dispatch delegates to wrap various execution parameters.
 */
USTRUCT()
struct ACTIONDISPATCH_API FGameActionContext
{
    GENERATED_USTRUCT_BODY()

public:
    FGameActionContext();
    FGameActionContext(const FGameAction& InAction);

    template<class ActionClass>
    const ActionClass* GetAction() const
    {
        return static_cast<const ActionClass*>(Action);
    }

    FGameActionContext& TargetPlayer(int32 InPlayerTargetId);
    FGameActionContext& ExcludePlayer(int32 InExcludePlayerId);
    FGameActionContext& ExecutedByPlayer(int32 InExecutedByPlayer);

    /**
     * ID of the player this action is targeting. Must be set for actions that
     * are executed with type Client
     */
    int32 TargetPlayerId;

    /**
     * ID of the player that is excluded by this action. Must be set for actions that
     * are executed with type Multicast.
     */
    int32 ExcludePlayerId;

    /**
     * Player id of the player that executed this action.
     * Only valid if the action was send by a client using an RPC channel.
     */
    int32 ExecutedByPlayerId;

    static const int32 PLAYER_ID_DEDICATED_SERVER = -100;
    static const int32 PLAYER_ID_UNSPECIFIED = -1;

private:
    const FGameAction* Action;
};

