#include "ActionDispatch.h"

#include "GameActionDispatcher.h"

AGameActionDispatcher::AGameActionDispatcher()
{
    bReplicates = true;
    bAlwaysRelevant = true;
}

void AGameActionDispatcher::ExecuteAction(ActionExecutionType Type, FGameActionContext ActionContext)
{
    ActionContext.ExecutedByPlayerId = GetExecutingPlayerId();

    switch (Type)
    {
    case Local:
        ExecuteLocal(ActionContext);
        break;

    case Server:
        ExecuteServer(ActionContext);
        break;

    case Net:
        ExecuteNet(ActionContext);
        break;

    case Client:
        ExecuteClient(ActionContext);
        break;

    case Multicast:
        ExecuteMulticast(ActionContext);
        break;

    default: break;
    }
}

void AGameActionDispatcher::ExecuteLocal(FGameActionContext& ActionContext)
{
    OnPreExecute(ActionContext);

    OnLocalExecute(ActionContext);

    OnPostExecute(ActionContext);
}

void AGameActionDispatcher::ExecuteNet(FGameActionContext& ActionContext)
{
    if (!HasAuthority())
    {
        ExecuteServer(ActionContext);
    }

    ExecuteLocal(ActionContext);
}

void AGameActionDispatcher::ExecuteServer(FGameActionContext& ActionContext)
{
    if (!HasAuthority())
    {
        OnServerExecute(ActionContext);
    }
    else
    {
        OnLocalExecute(ActionContext);
    }
}

void AGameActionDispatcher::ExecuteClient(FGameActionContext& ActionContext)
{
    if (ActionContext.ExecutedByPlayerId != ActionContext.TargetPlayerId)
    {
        OnClientExecute(ActionContext);
    }
    else
    {
        OnLocalExecute(ActionContext);
    }
}

void AGameActionDispatcher::ExecuteMulticast(FGameActionContext& ActionContext)
{
    if (HasAuthority())
    {
        for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; ++it)
        {
            int32 playerId = it->Get()->PlayerState->PlayerId;

            if (playerId != ActionContext.ExcludePlayerId)
            {
                ActionContext.TargetPlayerId = playerId;
                ExecuteClient(ActionContext);
            }
        }
    }
}

AGameActionDispatcher::FGameActionMultiDelegate& AGameActionDispatcher::GetPreExecutionDelegate(FName ActionName)
{
    return PreExecutionListenerMap.FindOrAdd(ActionName);
}

AGameActionDispatcher::FGameActionMultiDelegate& AGameActionDispatcher::GetPostExecutionDelegate(FName ActionName)
{
    return PostExecutionListenerMap.FindOrAdd(ActionName);
}

AGameActionDispatcher::FGameActionDelegate& AGameActionDispatcher::GetLocalExecutionDelegate(FName ActionName)
{
    return LocalExecutionListenerMap.FindOrAdd(ActionName);
}

AGameActionDispatcher::FGameActionDelegate& AGameActionDispatcher::GetServerExecutionDelegate(FName ActionName)
{
    return ServerExecutionListenerMap.FindOrAdd(ActionName);
}

AGameActionDispatcher::FGameActionDelegate& AGameActionDispatcher::GetClientExecutionDelegate(FName ActionName, int32 TargetPlayerId)
{
    TMap<int32, FGameActionDelegate>& PlayerDelegateMap = ClientExecutionListenerMap.FindOrAdd(ActionName);
    return PlayerDelegateMap.FindOrAdd(TargetPlayerId);
}

void AGameActionDispatcher::OnLocalExecute(const FGameActionContext& ActionContext)
{
    GetLocalExecutionDelegate(ActionContext.GetAction<FGameAction>()->GetActionName()).ExecuteIfBound(ActionContext);
}

void AGameActionDispatcher::OnServerExecute(const FGameActionContext& ActionContext)
{
    GetServerExecutionDelegate(ActionContext.GetAction<FGameAction>()->GetActionName()).ExecuteIfBound(ActionContext);
}

void AGameActionDispatcher::OnPreExecute(const FGameActionContext& ActionContext)
{
    GetPreExecutionDelegate(ActionContext.GetAction<FGameAction>()->GetActionName()).Broadcast(ActionContext);
}

void AGameActionDispatcher::OnPostExecute(const FGameActionContext& ActionContext)
{
    GetPostExecutionDelegate(ActionContext.GetAction<FGameAction>()->GetActionName()).Broadcast(ActionContext);
}

void AGameActionDispatcher::OnClientExecute(const FGameActionContext& ActionContext)
{
    GetClientExecutionDelegate(ActionContext.GetAction<FGameAction>()->GetActionName(), ActionContext.TargetPlayerId).ExecuteIfBound(ActionContext);
}

int32 AGameActionDispatcher::GetExecutingPlayerId() const
{
    //the first controller is always the controller of the local player, unless we are a dedicated server
    if (!HasAuthority() || GetNetMode() != NM_DedicatedServer)
    {
        return GetWorld()->GetFirstPlayerController()->PlayerState->PlayerId;
    }
    else
    {
        return FGameActionContext::PLAYER_ID_DEDICATED_SERVER;
    }
}
