#pragma once

#include "GameFramework/Actor.h"

#include "GameAction.h"
#include "GameActionContext.h"

#include "GameActionDispatcher.generated.h" 

/**
 * Dispatcher for game actions.
 * Allows easy centralized execution (locally, server only or both) and distribution of actions.
 * Supports listeners for pre execution, execution and post execution.
 * It is replicated per player and should be spawned by the server.
 */
UCLASS(ClassGroup = GameActionDispatch)
class ACTIONDISPATCH_API AGameActionDispatcher : public AActor
{
    friend class ARPCChannel;

    DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FGameActionMultiDelegate, FGameActionContext, action);
    DECLARE_DYNAMIC_DELEGATE_OneParam(FGameActionDelegate, FGameActionContext, action);

    GENERATED_BODY()

public:
    AGameActionDispatcher();

    /**
     * Executes a specific GameAction.
     *
     * @param Type Whether the action shall be executed locally only, on the server only or on both
     * @param ActionContext Context of the action to execute.
     *
     * @see UDispatch::ExecuteAction()
     */
    UFUNCTION(BlueprintCallable, Category = "GameActionDispatch")
    void ExecuteAction(ActionExecutionType Type, FGameActionContext ActionContext);

    /**
     * Retrieve the multi cast delegate for PRE action execution for a specific action identified by its name.
     * Called on local execution.
     *
     * @param ActionName The name (as FName) of the class whose delegate you want
     * @return A reference to the pre execution delegate of the class.
     *         Should only be used to bind functions, not for execution
     */
    FGameActionMultiDelegate& GetPreExecutionDelegate(FName ActionName);

    /**
     * Retrieve the multicast delegate for POST action execution for a specific action identified by its name.
     * Called on local execution.
     *
     * @param ActionName The name (as FName) of the class whose delegate you want
     * @return A reference to the post execution delegate of the class.
     *         Should only be used to bind functions, not for execution
     */
    FGameActionMultiDelegate& GetPostExecutionDelegate(FName ActionName);

    /**
     * Retrieve the delegate for LOCAL action execution for a specific action identified by its name.
     * Called on local execution.
     *
     * This is what the game action system should register for.
     *
     * @param ActionName The name (as FName) of the class whose delegate you want
     * @return A reference to the local execution delegate of the class.
     *         Should only be used to bind functions, not for execution
     */
    FGameActionDelegate& GetLocalExecutionDelegate(FName ActionName);

    /**
     * Retrieve the delegate for SERVER action execution for a specific action identified by its name.
     * Called on server execution.
     *
     * This is what RPC channels should register for to forward the call to a rpc method.
     *
     * @param ActionName The name (as FName) of the class whose delegate you want
     * @return A reference to the server execution delegate of the class.
     *         Should only be used to bind functions, not for execution
     */
    FGameActionDelegate& GetServerExecutionDelegate(FName ActionName);

    /**
    * Retrieve the delegate for CLIENT action execution for a specific action identified by its name.
    * Called on client execution.
    *
    * This is what RPC channels should register for to forward the call to a rpc method.
    *
    * @param actionName The name (as FName) of the class whose delegate you want
    * @return A reference to the client execution delegate of the class.
    *         Should only be used to bind functions, not for execution
    */
    FGameActionDelegate& GetClientExecutionDelegate(FName ActionName, int32 TargetPlayerId);

private:
    // wrapper methods for the execution types
    void ExecuteNet(FGameActionContext& ActionContext);
    void ExecuteLocal(FGameActionContext& ActionContext);
    void ExecuteServer(FGameActionContext& ActionContext);
    void ExecuteClient(FGameActionContext& ActionContext);
    void ExecuteMulticast(FGameActionContext& ActionContext);

    // methods for notification purposes
    void OnLocalExecute(const FGameActionContext& ActionContext);
    void OnServerExecute(const FGameActionContext& ActionContext);
    void OnPreExecute(const FGameActionContext& ActionContext);
    void OnPostExecute(const FGameActionContext& ActionContext);
    void OnClientExecute(const FGameActionContext& ActionContext);

    // Retrieve the player id of the player executing actions on this dispatcher
    int32 GetExecutingPlayerId() const;

    // maps assigning delegates to classes and execution types
    TMap<FName, FGameActionMultiDelegate> PreExecutionListenerMap;
    TMap<FName, FGameActionMultiDelegate> PostExecutionListenerMap;
    TMap<FName, FGameActionDelegate> LocalExecutionListenerMap;
    TMap<FName, FGameActionDelegate> ServerExecutionListenerMap;
    TMap<FName, TMap<int32, FGameActionDelegate> > ClientExecutionListenerMap;
};