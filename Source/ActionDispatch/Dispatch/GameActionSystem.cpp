#include "ActionDispatch.h"

#include "GameActionSystem.h"

#include "Dispatch.h"

AGameActionSystem::AGameActionSystem()
    : bBindingsInitialized(false)
{
    bReplicates = true;
    bAlwaysRelevant = true;

    PrimaryActorTick.bCanEverTick = true;
}

void AGameActionSystem::Tick(float DeltaSeconds)
{
    if (!bBindingsInitialized)
    {
        AGameActionDispatcher* dispatcher = UDispatch::GetGameActionDispatcher(GetWorld());

        if (dispatcher)
        {
            BindToActions(dispatcher);
            SetInitialized();
        }
    }
}

void AGameActionSystem::SetInitialized()
{
    bBindingsInitialized = true;
    PrimaryActorTick.bCanEverTick = false;
}
