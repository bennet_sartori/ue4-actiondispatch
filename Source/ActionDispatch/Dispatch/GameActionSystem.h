#pragma once

#include "GameFramework/Actor.h"

#include "GameActionDispatcher.h"
#include "GameActionContext.h"

#include "GameActionSystem.generated.h"

/**
 * Base class for a game action system. These systems are the suggested way to handle
 * the execution of the dispatched actions.
 * Create a subclass and override the BindToAction function to register for the different game actions.
 */
UCLASS(NotPlaceable, ClassGroup = GameActionDispatch)
class ACTIONDISPATCH_API AGameActionSystem : public AActor
{
    friend class ADefaultGameMode;

    GENERATED_BODY()

public:
    AGameActionSystem();

    virtual void Tick(float DeltaSeconds) override;

private:
    /**
     * Automatically executed on client and server to initialize the bindings to the dispatcher.
     * Override and register to your own actions.
     *
     * @param dispatcher The worlds action dispatcher that should be used for bindings.
     */
    virtual void BindToActions(AGameActionDispatcher* dispatcher) PURE_VIRTUAL(AGameActionSystem::BindToActions, );

    virtual void SetInitialized();

    bool bBindingsInitialized;
};
