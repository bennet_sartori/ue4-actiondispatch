#include "ActionDispatch.h"

#include "RPCChannel.h"

#include "Dispatch.h"

#include "UnrealNetwork.h"

ARPCChannel::ARPCChannel()
    : bBindingsInitialized(false)
    , OwningPlayerId(FGameActionContext::PLAYER_ID_UNSPECIFIED)
{
    bReplicates = true;
    bOnlyRelevantToOwner = true;

    PrimaryActorTick.bCanEverTick = true;
}

void ARPCChannel::Tick(float DeltaSeconds)
{
    if (!bBindingsInitialized)
    {
        AGameActionDispatcher* dispatcher = UDispatch::GetGameActionDispatcher(GetWorld());

        if (dispatcher)
        {
            if (HasAuthority())
            {
                BindToClientActions(dispatcher);
            }
            else
            {
                BindToServerActions(dispatcher);
            }

            SetInitialized();
        }
    }
}

void ARPCChannel::SetOwningPlayer(APlayerController* Player)
{
    SetOwner(Player);
    OwningPlayerId = Player->PlayerState->PlayerId;
}

void ARPCChannel::Execute(const FGameAction& Action)
{
    UDispatch::GetGameActionDispatcher(GetWorld())->ExecuteLocal(FGameActionContext(Action).ExecutedByPlayer(OwningPlayerId));
}

void ARPCChannel::SetInitialized()
{
    bBindingsInitialized = true;
    PrimaryActorTick.bCanEverTick = false;
}

void ARPCChannel::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME_CONDITION(ARPCChannel, OwningPlayerId, COND_InitialOnly);
}
