#pragma once

#include "GameFramework/Actor.h"

#include "GameActionDispatcher.h"
#include "GameActionContext.h"
#include "GameAction.h"

#include "RPCChannel.generated.h"

/**
* Base class for a rpc channel. A rpc channel is used to send action from client to server using unreals rpc mechanism.
* Create a subclass and override the bind functions to register for the server or client execution of game actions.
* 
* Helper macros:
* FORWARD_TO_RPC: Just use this inside your bound listener functions to call your defined RPC function for the action.
* DEFINE_RPC_IMPLEMENTATION: Creates the implementation for your defined rpc method.
*/
UCLASS(NotPlaceable, ClassGroup = GameActionDispatch)
class ACTIONDISPATCH_API ARPCChannel : public AActor
{
    GENERATED_BODY()

public:
    ARPCChannel();

    virtual void Tick(float DeltaSeconds) override;

    void SetOwningPlayer(APlayerController* Player);

protected:
    /**
     * An easy wrapper call to execute the action locally on the dispatcher.
     */
    void Execute(const FGameAction& Action);

    UPROPERTY(Replicated)
    int32 OwningPlayerId;

private:
    /**
    * Automatically executed on client only to initialize the bindings to the dispatcher.
    * Override and register to your own server actions.
    *
    * @param dispatcher The worlds action dispatcher that should be used for bindings
    */
    virtual void BindToServerActions(AGameActionDispatcher* dispatcher) PURE_VIRTUAL(ARPCChannel::BindToServerActions, );

    /**
    * Automatically executed on server only to initialize the bindings to the dispatcher.
    * Override and register to your own client actions.
    *
    * @param dispatcher The worlds action dispatcher that should be used for bindings
    */
    virtual void BindToClientActions(AGameActionDispatcher* dispatcher) PURE_VIRTUAL(ARPCChannel::BindToClientActions, );

    void SetInitialized();

    bool bBindingsInitialized;
};

/**
 * Helper that extracts the action and converts it to the correct type. Then calls the RPC function.
 *
 * @param RPCFuncName The name of your RPC function
 * @param ActionClass The concrete type of the game action struct to use
 * @param GameActionContext The instance of the game action context provided in the dispatch listener function
 */
#define FORWARD_TO_RPC(RPCFuncName, ActionClass, GameActionContext) \
    RPCFuncName##(*GameActionContext##.GetAction<ActionClass>());   

/**
 * Helper to create the implementation for the server RPC functions.
 * The RPC call simply calls Execute which forwards the action to the dispatcher and executes it locally.
 *
 * @param ClassScope The class your RPC function is defined in
 * @param RPCFuncName The name of your RPC function
 * @param ActionClass The concrete type of the game action struct to use.
 */
#define DEFINE_RPC_IMPLEMENTATION_SERVER(ClassScope, RPCFuncName, ActionClass) \
    bool ClassScope##::RPCFuncName##_Validate(ActionClass action) { return true; } \
    void ClassScope##::RPCFuncName##_Implementation(ActionClass action) { Execute(action); }

/**
* Helper to create the implementation for the client RPC functions.
* The RPC call simply calls Execute which forwards the action to the dispatcher and executes it locally.
*
* @param ClassScope The class your RPC function is defined in
* @param RPCFuncName The name of your RPC function
* @param ActionClass The concrete type of the game action struct to use.
*/
#define DEFINE_RPC_IMPLEMENTATION_CLIENT(ClassScope, RPCFuncName, ActionClass) \
    void ClassScope##::RPCFuncName##_Implementation(ActionClass action) { Execute(action); }