#include "UEActionDispatchTest.h"

#include "ColorMarkSystem.h"

#include "Actions/ColorMarkAction.h"

#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"

void AColorMarkSystem::OnColorMarkAction(FGameActionContext context)
{
    const FColorMarkAction* colorAction = context.GetAction<FColorMarkAction>();

    UActorComponent* component = colorAction->Actor->GetComponentByClass(UStaticMeshComponent::StaticClass());
    UStaticMeshComponent* meshComponent = CastChecked<UStaticMeshComponent>(component);
    check(meshComponent);
    
    UObject* object = StaticLoadObject(UMaterialInterface::StaticClass(), nullptr, (TEXT("Material'/Game/Materials/DefaultObjectMaterial.DefaultObjectMaterial'")));
    UMaterial* MatInstance = (UMaterial*)object;

    UMaterialInstanceDynamic* newMaterial = UMaterialInstanceDynamic::Create(MatInstance, this);
    newMaterial->SetVectorParameterValue("BaseColor", colorAction->MarkColor);
    meshComponent->SetMaterial(0, newMaterial);
}

void AColorMarkSystem::BindToActions(AGameActionDispatcher* dispatcher)
{
    dispatcher->GetLocalExecutionDelegate("ColorMarkAction").BindDynamic(this, &AColorMarkSystem::OnColorMarkAction);
}