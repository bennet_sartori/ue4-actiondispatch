#pragma once

#include "Dispatch/GameActionSystem.h"

#include "ColorMarkSystem.generated.h"

/**
 * 
 */
UCLASS()
class UEACTIONDISPATCHTEST_API AColorMarkSystem : public AGameActionSystem
{
    GENERATED_BODY()

private:
    UFUNCTION()
    void OnColorMarkAction(FGameActionContext context);

    virtual void BindToActions(AGameActionDispatcher* dispatcher) override;
};
