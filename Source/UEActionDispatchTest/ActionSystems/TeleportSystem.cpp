#include "UEActionDispatchTest.h"

#include "TeleportSystem.h"

#include "Actions/TeleportAction.h"

ATeleportSystem::ATeleportSystem()
{
    
}

void ATeleportSystem::BindToActions(AGameActionDispatcher* dispatcher)
{
    dispatcher->GetLocalExecutionDelegate("TeleportAction").BindDynamic(this, &ATeleportSystem::OnTeleportAction);
}

void ATeleportSystem::OnTeleportAction(FGameActionContext context)
{
    const FTeleportAction* teleportAction = context.GetAction<FTeleportAction>();

    teleportAction->Actor->SetActorLocation(teleportAction->TargetPosition);
}