#pragma once

#include "Dispatch/GameActionSystem.h"

#include "TeleportSystem.generated.h"

/**
 * 
 */
UCLASS()
class UEACTIONDISPATCHTEST_API ATeleportSystem : public AGameActionSystem
{
    GENERATED_BODY()

public:
    ATeleportSystem();

private:
    UFUNCTION()
    void OnTeleportAction(FGameActionContext context);

    virtual void BindToActions(AGameActionDispatcher* dispatcher) override;
};
