#include "UEActionDispatchTest.h"

#include "ThrowSystem.h"

#include "Actions/ThrowAction.h"

AThrowSystem::AThrowSystem()
{
    
}

void AThrowSystem::BindToActions(AGameActionDispatcher* dispatcher)
{
    dispatcher->GetLocalExecutionDelegate("ThrowAction").BindDynamic(this, &AThrowSystem::OnThrowAction);
}

void AThrowSystem::OnThrowAction(FGameActionContext context)
{
    const FThrowAction* throwAction = context.GetAction<FThrowAction>();

    UPrimitiveComponent* meshComponent = CastChecked<UPrimitiveComponent>(throwAction->Actor->GetRootComponent());
    if (meshComponent)
    {
        meshComponent->AddImpulse(FVector(0.0f, throwAction->Force.X, throwAction->Force.Y));
    }
}