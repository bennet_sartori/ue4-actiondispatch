#pragma once

#include "Dispatch/GameActionSystem.h"

#include "ThrowSystem.generated.h"

/**
 * 
 */
UCLASS()
class UEACTIONDISPATCHTEST_API AThrowSystem : public AGameActionSystem
{
    GENERATED_BODY()

public:
    AThrowSystem();

private:
    UFUNCTION()
    void OnThrowAction(FGameActionContext context);

    virtual void BindToActions(AGameActionDispatcher* dispatcher) override;
};
