#include "UEActionDispatchTest.h"

#include "ColorMarkAction.h"

FColorMarkAction::FColorMarkAction(AActor* InActor, FLinearColor InMarkColor)
{
    Actor = InActor;
    MarkColor = InMarkColor;
}

FColorMarkAction::FColorMarkAction()
{

}

FName FColorMarkAction::GetActionName() const
{
    return FName(TEXT("ColorMarkAction"));
}
