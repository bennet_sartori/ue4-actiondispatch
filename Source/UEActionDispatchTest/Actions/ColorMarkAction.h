#pragma once

#include "Dispatch/GameAction.h"

#include "ColorMarkAction.generated.h"

/**
 * 
 */
USTRUCT()
struct UEACTIONDISPATCHTEST_API FColorMarkAction : public FGameAction
{
    GENERATED_USTRUCT_BODY()
    
public:
    FColorMarkAction();
    FColorMarkAction(AActor* InActor, FLinearColor InMarkColor);

    UPROPERTY()
    TWeakObjectPtr<AActor> Actor;

    UPROPERTY()
    FLinearColor MarkColor;

    virtual FName GetActionName() const override;
};
