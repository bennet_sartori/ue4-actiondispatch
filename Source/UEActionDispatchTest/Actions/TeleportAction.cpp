#include "UEActionDispatchTest.h"

#include "TeleportAction.h"

FTeleportAction::FTeleportAction(AActor* InActor, FVector InPosition)
{
    Actor = InActor;
    TargetPosition = InPosition;
}

FTeleportAction::FTeleportAction()
{

}

FName FTeleportAction::GetActionName() const
{
    return FName(TEXT("TeleportAction"));
}
