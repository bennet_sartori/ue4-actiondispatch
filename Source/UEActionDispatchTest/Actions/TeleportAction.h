#pragma once

#include "Dispatch/GameAction.h"

#include "TeleportAction.generated.h"

/**
 * 
 */
USTRUCT()
struct UEACTIONDISPATCHTEST_API FTeleportAction : public FGameAction
{
    GENERATED_USTRUCT_BODY()
    
public:
    FTeleportAction();
    FTeleportAction(AActor* InActor, FVector InPosition);

    UPROPERTY()
    TWeakObjectPtr<AActor> Actor;

    UPROPERTY()
    FVector TargetPosition;

    virtual FName GetActionName() const override;
};
