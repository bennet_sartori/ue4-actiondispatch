#include "UEActionDispatchTest.h"

#include "ThrowAction.h"

FThrowAction::FThrowAction(AActor* InActor, FVector2D InForce)
{
    Actor = InActor;
    Force = InForce;
}

FThrowAction::FThrowAction()
{

}

FName FThrowAction::GetActionName() const
{
    return FName(TEXT("ThrowAction"));
}
