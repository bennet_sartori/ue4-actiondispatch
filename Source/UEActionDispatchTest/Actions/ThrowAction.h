#pragma once

#include "Dispatch/GameAction.h"

#include "ThrowAction.generated.h"

/**
 * 
 */
USTRUCT()
struct UEACTIONDISPATCHTEST_API FThrowAction : public FGameAction
{
    friend class GameActionDispatcher;

    GENERATED_USTRUCT_BODY()
    
public:
    FThrowAction();
    FThrowAction(AActor* InActor, FVector2D InForce);

    UPROPERTY()
    TWeakObjectPtr<AActor> Actor;

    UPROPERTY()
    FVector2D Force;

    virtual FName GetActionName() const override;
};
