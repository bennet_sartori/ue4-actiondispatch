#include "UEActionDispatchTest.h"

#include "DefaultBallController.h"

#include "Dispatch/Dispatch.h"
#include "Actions/ThrowAction.h"
#include "Actions/TeleportAction.h"
#include "Actions/ColorMarkAction.h"

ADefaultBallController::ADefaultBallController(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    bShowMouseCursor = true;
}

void ADefaultBallController::SetupInputComponent()
{
    Super::SetupInputComponent();

    InputComponent->BindAction("InputThrow", IE_Pressed, this, &ADefaultBallController::OnThrowBegin);
    InputComponent->BindAction("InputThrow", IE_Released, this, &ADefaultBallController::OnThrowEnd);

    InputComponent->BindAction("InputTeleport", IE_Pressed, this, &ADefaultBallController::OnTeleport);

    InputComponent->BindAction("InputColor", IE_Pressed, this, &ADefaultBallController::OnColor);
}

void ADefaultBallController::OnThrowBegin()
{
    float x, y;
    GetMousePosition(x, y);
    StartPosition = FVector2D(x, y);
}

void ADefaultBallController::OnThrowEnd()
{
    FVector2D force = calculateForce();

    UDispatch::ExecuteAction(GetWorld(),
        ActionExecutionType::Net,
        FGameActionContext(FThrowAction(GetPawn(), force)));
}

void ADefaultBallController::OnTeleport()
{
    UDispatch::ExecuteAction(GetWorld(),
        ActionExecutionType::Local, 
        FGameActionContext(FTeleportAction(GetPawn(), FVector(0, 0, GetPawn()->GetActorLocation().Z))));
}

void ADefaultBallController::OnColor()
{
    FLinearColor color = calculateRandomColor();
    AActor* actor = calculateActorToColor();

    if (HasAuthority() && GetWorld()->GetAuthGameMode()->GetNumPlayers() > 1)
    {
        int32 randClientPlayerId = calculateRandomClient();

        UDispatch::ExecuteAction(GetWorld(),
            ActionExecutionType::Multicast,
            FGameActionContext(FColorMarkAction(actor, color)).ExcludePlayer(PlayerState->PlayerId));
    }
    else
    {
        UDispatch::ExecuteAction(GetWorld(), ActionExecutionType::Server, FColorMarkAction(actor, color));
    }
}

int32 ADefaultBallController::calculateRandomClient()
{
    int32 randPlayerIndex = FMath::Max(FMath::Rand() % GetWorld()->GetAuthGameMode()->GetNumPlayers(), 1);
    int32 playerId = -1;
    for (auto it = ++GetWorld()->GetPlayerControllerIterator(); it; ++it)
    {
        if (it.GetIndex() == randPlayerIndex)
        {
            playerId = it->Get()->PlayerState->PlayerId;
            break;
        }
    }

    return playerId;
}

AActor* ADefaultBallController::calculateActorToColor()
{
    static int returnActor = 0;
    static int maxActors = 2;

    int index = 0;

    AActor* actor = nullptr;
    for (TActorIterator<AStaticMeshActor> it(GetWorld()); it; ++it)
    {
        AActor* itActor = *it;
        bool isColorable = itActor->GetName().Contains("Wall");
        if (isColorable)
        {
            if ((returnActor = returnActor % maxActors) == index)
            {
                actor = *it;
                returnActor++;
                break;
            }

            index++;
        }
    }

    return actor;
}

FLinearColor ADefaultBallController::calculateRandomColor()
{
    FLinearColor color = FLinearColor((float)FMath::Rand() / RAND_MAX,
                                      (float)FMath::Rand() / RAND_MAX,
                                      (float)FMath::Rand() / RAND_MAX);

    return color;
}

FVector2D ADefaultBallController::calculateForce()
{
    float x, y;
    GetMousePosition(x, y);
    FVector2D EndPosition = FVector2D(x, y);

    FVector2D force = EndPosition - StartPosition;
    force.Y = -1 * force.Y;

    force.X = FMath::Clamp(force.X, -2000.f, 2000.f);
    force.Y = FMath::Clamp(force.Y, -2000.f, 2000.f);

    return force;
}
