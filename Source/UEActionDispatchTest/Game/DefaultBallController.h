#pragma once

#include "GameFramework/PlayerController.h"

#include "DefaultBallController.generated.h"

class AGameActionDispatcher;

/**
 * 
 */
UCLASS()
class UEACTIONDISPATCHTEST_API ADefaultBallController : public APlayerController
{
    GENERATED_UCLASS_BODY()

public:
    void SetupInputComponent() override;

private:
    void OnThrowBegin();
    void OnThrowEnd();
    void OnTeleport();
    void OnColor();

    int32 calculateRandomClient();
    AActor* calculateActorToColor();
    FLinearColor calculateRandomColor();
    FVector2D calculateForce();
    
    FVector2D StartPosition;
};
