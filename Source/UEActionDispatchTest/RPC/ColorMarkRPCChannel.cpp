#include "UEActionDispatchTest.h"

#include "Actions/ColorMarkAction.h"

#include "ColorMarkRPCChannel.h"

DEFINE_RPC_IMPLEMENTATION_SERVER(AColorMarkRPCChannel, ColorMarkServer, FColorMarkAction);
DEFINE_RPC_IMPLEMENTATION_CLIENT(AColorMarkRPCChannel, ColorMarkClient, FColorMarkAction);

void AColorMarkRPCChannel::BindToServerActions(AGameActionDispatcher* dispatcher)
{
    dispatcher->GetServerExecutionDelegate("ColorMarkAction").BindDynamic(this, &AColorMarkRPCChannel::OnColorMarkActionServer);
}

void AColorMarkRPCChannel::BindToClientActions(AGameActionDispatcher* dispatcher)
{
    dispatcher->GetClientExecutionDelegate("ColorMarkAction", OwningPlayerId).BindDynamic(this, &AColorMarkRPCChannel::OnColorMarkActionClient);
}

void AColorMarkRPCChannel::OnColorMarkActionServer(FGameActionContext context)
{
    FORWARD_TO_RPC(ColorMarkServer, FColorMarkAction, context);
}

void AColorMarkRPCChannel::OnColorMarkActionClient(FGameActionContext context)
{
    FORWARD_TO_RPC(ColorMarkClient, FColorMarkAction, context);
}