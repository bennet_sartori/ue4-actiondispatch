#pragma once

#include "Dispatch/RPCChannel.h"

#include "Actions/ColorMarkAction.h"

#include "ColorMarkRPCChannel.generated.h"

class AGameActionDispatcher;

/**
 * 
 */
UCLASS()
class UEACTIONDISPATCHTEST_API AColorMarkRPCChannel : public ARPCChannel
{
    GENERATED_BODY()

private:
    virtual void BindToServerActions(AGameActionDispatcher* dispatcher) override;
    virtual void BindToClientActions(AGameActionDispatcher* dispatcher) override;

    UFUNCTION()
    void OnColorMarkActionServer(FGameActionContext context);
    UFUNCTION()
    void OnColorMarkActionClient(FGameActionContext context);

    UFUNCTION(Server, Reliable, WithValidation)
    void ColorMarkServer(FColorMarkAction action);

    UFUNCTION(Client, Reliable)
    void ColorMarkClient(FColorMarkAction action);
};
