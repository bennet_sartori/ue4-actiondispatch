#include "UEActionDispatchTest.h"

#include "ThrowRPCChannel.h"

DEFINE_RPC_IMPLEMENTATION_SERVER(AThrowRPCChannel, ThrowServer, FThrowAction);

void AThrowRPCChannel::BindToServerActions(AGameActionDispatcher* dispatcher)
{
    dispatcher->GetServerExecutionDelegate("ThrowAction").BindDynamic(this, &AThrowRPCChannel::OnThrowAction);
}

void AThrowRPCChannel::BindToClientActions(AGameActionDispatcher* dispatcher)
{
    // no client actions
}

void AThrowRPCChannel::OnThrowAction(FGameActionContext context)
{
    FORWARD_TO_RPC(ThrowServer, FThrowAction, context);
}

