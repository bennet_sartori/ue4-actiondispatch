#pragma once

#include "Dispatch/RPCChannel.h"

#include "Actions/ThrowAction.h"

#include "ThrowRPCChannel.generated.h"

class AGameActionDispatcher;

/**
 * 
 */
UCLASS()
class UEACTIONDISPATCHTEST_API AThrowRPCChannel : public ARPCChannel
{
    GENERATED_BODY()

private:
    virtual void BindToServerActions(AGameActionDispatcher* dispatcher) override;
    virtual void BindToClientActions(AGameActionDispatcher* dispatcher) override;

    UFUNCTION()
    void OnThrowAction(FGameActionContext context);

    UFUNCTION(Server, Reliable, WithValidation)
    void ThrowServer(FThrowAction action);
};
