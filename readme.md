# ActionDispatch - Unreal Engine 4 Module for generic inter-class communication

## What it is:
- A centralized network capable dispatcher to send and listen to actions
- Setup action handling once and use the same flow on Client and Server without much extra effort
- Unreal Engine 4 Module, easy to integrate into your project

## Class Overview:
### FGameAction:
- Base class used for all user defined actions
- Create your own action by simply subclassing it
- Container for the action parameters
- Is not concerned with the execution

### FGameActionContext:
- Wrapper to the game action capturing some more context
- Currently the context also contains the player that executed the action - useful on server to distinguish actions by client
- Could easily be extended by the user to include more context

### AGameActionDispatcher:
- The central dispatcher for actions
- Sends actions and allows for delegates to register to the different execution states (pre, execute, post, server)
- Registration is done on per action basis
- Action can be executed local only, server only or on both
- Write just one line to start the action flow
- Replicated actor (no internal state is replicated) - thus one copy exists on server and all clients

### AGameActionSystem:
- Recommended way to handle the real execution of actions
- Subclass and override one method to bind to the actions you want to handle
- Callbacks are automatically called when bound and the respecting action is executed
- System - Action Relation should be 1:1 but thats up to you - You could also write a monolithic system that handles everything
- Replicated actor (no internal state is replicated) - thus one copy exists on server and all clients

### ARPCChannel:
- Communication channel to send actions that need server execution from client to server using RPC calls
- Subclass and override one method (same as system) - In callback just forward the action to your defined RPC call
- One for each player (owned by the player)
- RPC channel - Action Relation should be 1:1 but thats up to you - You could also write a monolithic channel that handles everything

### UDispatch:
- Helper to easy the usage

## Action flow diagram:
CHECK THE DOCS folder

## How to setup and use (code example):
### Step 1:
Spawn the dispatcher (I recommend doing it when starting your game mode).

	void ADefaultGameMode::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage)
	{
	    Super::InitGame(MapName, Options, ErrorMessage);

	    UDispatch::SpawnDispatcher(GetWorld());
	}

### Step 2:
Create your own action.

	USTRUCT()
	struct UEACTIONDISPATCHTEST_API FTeleportAction : public FGameAction
	{
	    GENERATED_USTRUCT_BODY()
	    
	public:
	    FTeleportAction();
	    FTeleportAction(AActor* InActor, FVector InPosition);

	    UPROPERTY()
	    TWeakObjectPtr<AActor> Actor;

	    UPROPERTY()
	    FVector TargetPosition;

	    virtual FName getActionName() const override
	    {
	    	return "ThrowAction";
	    };
	};

### Step 3:
Create your game system.

	UCLASS()
	class UEACTIONDISPATCHTEST_API AThrowSystem : public AGameActionSystem
	{
	    GENERATED_BODY()

	public:
	    AThrowSystem();

	private:
	    UFUNCTION()
	    void OnThrowAction(FGameActionContext context)
	    {
	        const FThrowAction* throwAction = context.GetAction<FThrowAction>();

    		UPrimitiveComponent* meshComponent = CastChecked<UPrimitiveComponent>(throwAction->Actor->GetRootComponent());
    		if (meshComponent)
		    {
		        meshComponent->AddImpulse(FVector(0.0f, throwAction->Force.X, throwAction->Force.Y));
		    }
	    };

	    virtual void BindToActions(AGameActionDispatcher* dispatcher) override
	    {
	    	dispatcher->GetLocalExecutionDelegate("ThrowAction").BindDynamic(this, &AThrowSystem::OnThrowAction);
	    };
	};

Spawn the game system.

	void ADefaultGameMode::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage)
	{
	    Super::InitGame(MapName, Options, ErrorMessage);

	    UDispatch::SpawnDispatcher(GetWorld());

	    GetWorld()->SpawnActor<AThrowSystem>();
	}

### Step 4:
Create your RPC channel.

	UCLASS()
	class UEACTIONDISPATCHTEST_API AThrowRPCChannel : public ARPCChannel
	{
	    GENERATED_BODY()

	private:
	    virtual void BindToActions(AGameActionDispatcher* dispatcher) override
	    {
			dispatcher->GetServerExecutionDelegate("ThrowAction").BindDynamic(this, &AThrowRPCChannel::OnThrowAction);
	    };

	    UFUNCTION()
	    void OnThrowAction(FGameActionContext context)
	    {
			FORWARD_TO_RPC(ThrowServer, FThrowAction, context);
	    };

	    UFUNCTION(Server, Reliable, WithValidation)
	    void ThrowServer(FThrowAction action);
	};

	DEFINE_RPC_IMPLEMENTATION(AThrowRPCChannel, ThrowServer, FThrowAction);

Spawn your RPC channels for each player.

	void ADefaultGameMode::StartNewPlayer(APlayerController* NewPlayer)
	{
	    TArray<TSubclassOf<ARPCChannel> > channels;
	    channels.Add(ATeleportRPCChannel::StaticClass());

	    UDispatch::SpawnRPCChannels(NewPlayer, channels);
	}

### Step 5:
Execute the action from anywhere (example PlayerController).

	UDispatch::ExecuteAction(GetWorld(), ActionExecutionType::Net, FGameActionContext(FThrowAction(GetPawn(), FVector2D(100.f, 100.f)));